import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from "../components/Layouts";
import Link from "next/link";
export default function Home({data}) {
  return (
      <Layout page={"crypto !" + data.name } >
          <div className="row">
              {
                  data.map((crypto,index)=>(
                      <div key={index} className="col-sm border border-primary mr-3 rounded bg-secondary" >
                          <div className="mt-3 ">
                              <Link href={`/${crypto.id}`}>
                                  <a className="rounded-sm pt-3">
                                      <img src={crypto.logo_url } alt={ crypto.name } width="250" height="200"/>
                                  </a>
                              </Link>
                          </div>

                          <h2 className="text-center text-danger text-justify mt-5">
                              {crypto.name}
                          </h2>
                          <p className="mt-5 text-center text-muted">
                              {parseFloat(crypto.price).toFixed(2)} EUROS
                          </p>
                      </div>
                  ))
              }
          </div>
      </Layout>
  )
}

//server rendered

export async function getStaticProps(context) {
    const res = await fetch(`https://api.nomics.com/v1/currencies/ticker?key=9635ccf29958f878cf6afb9b043a5fb0&ids=BTC,ETH,XRP&interval=1d,30d,365d&convert=EUR&attributes=id,name,description,reddit_url`)
    const data = await res.json()

    if (!data) {
        return {
            notFound: true,
        }
    }

    return {
        props: {
            data
        }, // will be passed to the page component as props
    }
}
