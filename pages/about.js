import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from "../components/Layouts";

export default function About() {
    return (
        <Layout page={"crypto ! Accueil"} >
            <h1 className="text-center">A propos</h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi doloremque, error possimus praesentium quas qui? Accusamus debitis deleniti dicta dolore ducimus magni nemo neque nisi odit, officia quas soluta suscipit!
            </p>
        </Layout>
    )
}