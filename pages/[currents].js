import Layout from "../components/Layouts";

export default function Currents ({ data}) {
    {
        var page = '';
        var id = '';
        var logo_url = '';
        var desc = '';
        console.log(data)
        data.map((crypto,index)=>(
            page = crypto.name,
            logo_url= crypto.logo_url,
            id = index,
            desc = crypto.description

        ));
    }
    return(
        <Layout  page={'page ' + page}>
            <h1 className="text-center m-5 ">{page}</h1>
            <div key={id} className="col-sm border border-primary mr-3 rounded bg-secondary" >
                <img src={logo_url} alt={ page } width="1000" height="300"/>
                <p>{desc} </p>
            </div>

        </Layout>
    );
}

export async function getServerSideProps({query}) {
    //console.log(query.currents)
    const res = await fetch(`https://api.nomics.com/v1/currencies/ticker?key=9635ccf29958f878cf6afb9b043a5fb0&ids=${query.currents}&interval=1d,30d,365d&convert=EUR&attributes=id,name,description,reddit_url`)
    const data = await res.json()

    if (!data) {
        return {
            notFound: true,
        }
    }

    return {
        props: {
            data: data
        },
    }
}