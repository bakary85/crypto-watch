import Head from "next/head";
import Link from "next/link";
import {Image} from "react-bootstrap";
export default function Layout({children, page}){
    return(
        <div className="container">
            <Head>
                <title>{page}</title>
            </Head>
            <header>
                <div className="d-flex flex-row mb-5">
                    <Link href="/">
                        <button className="btn btn-secondary mr-5">
                            Accueil
                        </button>
                    </Link>
                    <Link href="/about">
                        <button className="btn btn-secondary ml-5 ">
                            A propos
                        </button>
                    </Link>
                </div>
            </header>
            <main className="min-vh-100">
                {children}
            </main>

            <footer className="w-100 fixed-bottom ">
                <Image src="/ban.png" width="100%"  height="100"/>
            </footer>
        </div>
    )
}